require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  if arr == []
    return 0
  end
  arr.reduce(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_string? helper method
def in_all_strings?(long_strings, substring)
 long_strings.all? {|string| string.include?(substring)}
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
characters = string.chars.uniq
characters.delete(" ")
characters.select {|char| string.count(char) > 1}
end


# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  organized_string = string.split(" ").sort_by {|word| word.length}
  organized_string[-2..-1].reverse
end



# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  alphabet = ("a".."z")
  missing_arr= []
 alphabet.each do |i|

  missing_arr << i if  !(string.split("").include?(i))
end
missing_arr
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
return_range = []

(first_yr ..last_yr).each do |i|
  if not_repeat_year?(i)
    return_range << i
  end
end
return_range
end

def not_repeat_year?(year)

year.to_s.split("").uniq == year.to_s.split("")
end



# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
 # appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  uniq_songs = songs.uniq
  uniq_songs.select do |song|
    no_repeats?(song, songs)
  end
end

def no_repeats?(song_name, songs)
  songs.each_with_index do |song, idx|
    if song == song_name
      return false if song == songs[idx + 1]
    end
  end

  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  string1 = remove_punctuation(string)
  c_words = string1.split.select { |word| word.downcase.include?("c") }

  return "" if c_words.empty?
   max_value = c_words.sort_by { |word| c_distance(word) }.min

  end


def remove_punctuation(string)
string.delete(",.;:?!")
end


def c_distance(word)
word.reverse.index("c")
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]
#
# def repeated_number_ranges(arr)
#
#   ranges = []
#     start_index = nil
#
#     # start walking
#     # set the start_index when we're at the beginning of a range
#     # when we reach the end of a range, add the range to the list and reset the start_index
#
#     numbers.each_with_index do |el, idx|
#       next_el = numbers[idx + 1]
#       if el == next_el
#         start_index = idx unless start_index #i.e., reset the start_index if it's nil
#       elsif start_index # i.e., if the start index isn't nil (the numbers switched)
#         ranges.push([start_index, idx])
#         start_index = nil # reset the start_index to nil so we can capture more ranges
#       end
#     end
#
#     ranges
#
# end

def repeated_number_ranges(arr)
range = []
start= nil
later = 0
arr.each_with_index do |el, idx|
if arr[idx] == arr[idx +1]
  p start = idx if start == nil
if arr[idx] == arr[idx + 1] && arr[idx +1] != arr[idx + 2]
later = idx + 1
range << [start, later]
start = nil
end
end
end
range
end
